<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['namespace' => 'Site'], function () {

    // Institucional
    Route::get('/', ['as' => 'site.index', 'uses' => 'InstitucionalController@index']);
    Route::get('/contato', ['as' => 'site.contact', 'uses' => 'InstitucionalController@contact']);

    // Authentication Routes...
    Route::get('/login', ['as' => 'site.show.login' ,'uses' => 'LoginController@showLoginForm']);
    Route::post('/login', ['as' => 'site.login', 'uses' => 'LoginController@login']);
    Route::get('/logout', ['as' => 'site.logout', 'uses' => 'LoginController@logout']);

    // Registration Routes...
    Route::get('/registrar', ['as' => 'site.show.register', 'uses' => 'RegisterController@showRegistrationForm']);
    Route::post('/registrar', ['as' => 'site.register', 'uses' => 'RegisterController@save']);

    // site news
    Route::get('/noticias', ['as' => 'site.all.news', 'uses' => 'NewsController@allNews']);
    Route::get('/noticia/{id}', ['as' => 'site.news.id', 'uses' => 'NewsController@news']);

    // site tourism
    Route::get('/turismos', ['as' => 'site.all.tourism', 'uses' => 'TourismController@allTourism']);
    Route::get('/turismo/{id}', ['as' => 'site.tourism.id', 'uses' => 'TourismController@tourism']);

    // site utility
    Route::get('/utilidades/horario-onibus', ['as' => 'site.utility.bus.time', 'uses' => 'UtilityController@busTime']);

    // site admin
    Route::group(['prefix' => 'administracao', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

        //site admin dashboard
        Route::get('/inicio', ['as' => 'site.admin.index', 'uses' => 'DashboardController@index']);

        // site user profile
        Route::get('/user/profile/{id}', ['as' => 'site.user.profile', 'uses' => 'UserController@userProfile']);
        Route::get('/user/profile/edit/{id}', ['as' => 'site.admin.user.profile.edit', 'uses' => 'UserController@userProfileEdit']);
        Route::post('/user/profile//edit{id}', ['as' => 'site.admin.user.profile.update', 'uses' => 'UserController@userProfileUpdate']);
        Route::get('/user/delete/{id}', ['as' => 'site.admin.user.delete', 'uses' => 'UserController@delete']);

    });

});

Route::group(['middleware' => 'web' ,'prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::auth();

    Route::get('/login', ['as' => 'admin.login', 'uses' => 'LoginController@index']);
    Route::post('/login', ['as' => 'admin.authenticate', 'uses' => 'LoginController@authenticate']);
    Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'LoginController@logout']);

    Route::group(['middleware' => 'auth'], function () {

        // Dashboard Admin
        Route::get('/dashboard', ['as' => 'admin.index', 'uses' => 'DashboardController@index']);

        // User Admin Account
        Route::get('/user/profile/edit/{id}', ['as' => 'admin.user.profile.edit', 'uses' => 'UserAdminController@userProfileEdit']);
        Route::post('/user/profile//edit{id}', ['as' => 'admin.user.profile.update', 'uses' => 'UserAdminController@userProfileUpdate']);

        // Users
        Route::get('/users/list', ['as' => 'admin.users.list', 'uses' => 'UserAdminController@usersList']);
        Route::get('/users/create', ['as' => 'admin.users.create', 'uses' => 'UserAdminController@create']);
        Route::post('/users/create', ['as' => 'admin.users.save', 'uses' => 'UserAdminController@save']);
        Route::get('/users/delete/{id}', ['as' => 'admin.users.delete', 'uses' => 'UserAdminController@delete']);

        // System Profile
        Route::get('/system/profile/list', ['as' => 'admin.system.profile.list', 'uses' => 'SystemProfileController@systemProfileList']);
        Route::get('/system/profile/create', ['as' => 'admin.system.profile.create', 'uses' => 'SystemProfileController@systemProfileCreate']);
        Route::post('/system/profile/save/', ['as' => 'admin.system.profile.save', 'uses' => 'SystemProfileController@systemProfileSave']);
        Route::get('/system/profile/edit/{id}', ['as' => 'admin.system.profile.edit', 'uses' => 'SystemProfileController@systemProfileEdit']);
        Route::post('/system/profile/update/{id}', ['as' => 'admin.system.profile.update', 'uses' => 'SystemProfileController@systemProfileUpdate']);
        Route::get('/system/profile/delete/{id}', ['as' => 'admin.system.profile.delete', 'uses' => 'SystemProfileController@systemProfileDelete']);

        // News
        Route::group(['prefix' => 'news'], function () {
            Route::get('/list', ['as' => 'admin.news.list', 'uses' => 'NewsController@list']);
            Route::get('/create', ['as' => 'admin.news.create', 'uses' => 'NewsController@create']);
            Route::post('/save', ['as' => 'admin.news.save', 'uses' => 'NewsController@save']);
            Route::get('/edit/{id}', ['as' => 'admin.news.edit', 'uses' => 'NewsController@edit']);
            Route::post('/update/{id}', ['as' => 'admin.news.update', 'uses' => 'NewsController@update']);
            Route::get('/delete/{id}', ['as' => 'admin.news.delete', 'uses' => 'NewsController@delete']);

        // Category News
            Route::get('/category/list', ['as' => 'admin.news.category.list', 'uses' => 'NewsCategoryController@list']);
            Route::get('/category/create', ['as' => 'admin.news.category.create', 'uses' => 'NewsCategoryController@create']);
            Route::post('/category/save', ['as' => 'admin.news.category.save', 'uses' => 'NewsCategoryController@save']);
            Route::get('/category/edit/{id}', ['as' => 'admin.news.category.edit', 'uses' => 'NewsCategoryController@edit']);
            Route::post('/category/update/{id}', ['as' => 'admin.news.category.update', 'uses' => 'NewsCategoryController@update']);
            Route::get('/category/delete/{id}', ['as' => 'admin.news.category.delete', 'uses' => 'NewsCategoryController@delete']);
        });

        // Tourism
        Route::group(['prefix' => 'tourism'], function () {
            Route::get('/list', ['as' => 'admin.tourism.list', 'uses' => 'TourismController@list']);
            Route::get('/create', ['as' => 'admin.tourism.create', 'uses' => 'TourismController@create']);
            Route::post('/save', ['as' => 'admin.tourism.save', 'uses' => 'TourismController@save']);
            Route::get('/edit/{id}', ['as' => 'admin.tourism.edit', 'uses' => 'TourismController@edit']);
            Route::post('/update/{id}', ['as' => 'admin.tourism.update', 'uses' => 'TourismController@update']);
            Route::get('/delete/{id}', ['as' => 'admin.tourism.delete', 'uses' => 'TourismController@delete']);

        // Category Tourism
            Route::get('/category/list', ['as' => 'admin.tourism.category.list', 'uses' => 'TourismCategoryController@list']);
            Route::get('/category/create', ['as' => 'admin.tourism.category.create', 'uses' => 'TourismCategoryController@create']);
            Route::post('/category/save', ['as' => 'admin.tourism.category.save', 'uses' => 'TourismCategoryController@save']);
            Route::get('/category/edit/{id}', ['as' => 'admin.tourism.category.edit', 'uses' => 'TourismCategoryController@edit']);
            Route::post('/category/update/{id}', ['as' => 'admin.tourism.category.update', 'uses' => 'TourismCategoryController@update']);
            Route::get('/category/delete/{id}', ['as' => 'admin.tourism.category.delete', 'uses' => 'TourismCategoryController@delete']);
        });

    });

});
