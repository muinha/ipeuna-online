$(document).ready(function(){

    $('#form_site_create_user').validate({

        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 80
            },
            username: {
                required: true,
                minlength: 6,
                maxlength: 80
            }
        }
    
    });
    
    $('#site_login_form_admin').validate({
    
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true
            }
        },
    });

});