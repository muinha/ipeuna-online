$(document).ready(function() {

	$('#cep').mask('99999-999');

	$('#cep').focusout(function () {
		var cep = $(this).val();
		cep = cep.replace(/[\D]/g,'');
		if (cep.length !== 8) {
			return;
		}

		$.ajax({
			url: 'https://viacep.com.br/ws/'+cep+'/json/',
			dataType    : "json",
			contentType : "application/json",
			beforeSend: function() {
                $('#form_admin_create_users').isLoading({
                    position: "overlay"
                });
			},
			success: function(saida) {
                $('#form_admin_create_users').isLoading('hide');

				if(saida['erro'] === true){
					$('#toast-container').removeClass('hide');

					setTimeout(function() {
						$('.toast-container').fadeOut(1000);
					}, 5000); // <-- time in milliseconds

					$('#cep').css('color', 'red');
					$('#cep').css('border-color', 'red');
				} else {
					$('#cep').css('color', 'inherit');
					$('#cep').css('border-color', 'green');
				}

				var logradouro = saida['logradouro'];
				var localidade = saida['localidade'];
				var estado = saida['uf'];

				$('#address').val(logradouro);
				$('#city').val(localidade);
				$('#states').val(estado);
			}
		});
	});

});