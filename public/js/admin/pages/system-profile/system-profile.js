$(document).ready(function() {
    
    $("#table_system_profile").DataTable({
        "language" : {
            "url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
        },
        "info": false
    });

    $('#form_system_profile').validate({
        rules: {
            name: {
                required: true
            }
        }
    });

});