@if($errors->any())
    <ol style="margin: 5px;" class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ol>
    <div id="toast-container" class="toast-container toast-top-right">
        <div class="toast toast-error" aria-live="polite" style="">
            <div class="toast-title">Erro</div>
        </div>
    </div>
@endif

@if(session('error'))
    <!-- Dissmissal Alert -->
    <div class="alert alert-danger"> <i class="ti-user"></i> {{ session('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div id="toast-container" class="toast-container toast-top-right">
        <div class="toast toast-error" aria-live="polite" style="">
            <div class="toast-title">Erro</div>
        </div>
    </div>
@endif

@if(session('success'))
    <!-- Dissmissal Alert -->
    <div id="toast-container" class="toast-container toast-top-right">
        <div class="toast toast-success" aria-live="polite" style="">
            <div class="toast-title">Sucesso</div>
            <div class="toast-message">{{ session('success') }}</div>
        </div>
    </div>
@endif

<!-- Dissmissal Alert -->
<div id="toast-container" class="toast-container toast-top-right hide">
    <div class="toast toast-error" aria-live="polite" style="">
        <div class="toast-title">Erro</div>
        <div class="toast-message">Cep Inválido</div>
    </div>
</div>


<script>
    setTimeout(function() {
        $('.toast-container').fadeOut(1000);
    }, 5000); // <-- time in milliseconds

    setTimeout(function() {
        $('.alert').fadeOut(1000);
    }, 10000); // <-- time in milliseconds
</script>