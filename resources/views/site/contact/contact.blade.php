@extends('site.template.layout')

@section('title', 'Contato')

@section('content')

<div class="site-contact-title container-fluid">
    <h1>Página de Contato</h1>
</div>

<div class="site-contact container">
    <div class="row">
        <div style="margin:auto;" class="col-md-6">
            <img class="img-thumbnail" src="{{ asset('/imagens/carta.jpg') }}" alt="Carta">
        </div>
        <div class="col-md-6">
            <form action="" method="post">
                <div class="form-group">
                    <label for="name">Digite seu nome:</label>
                    <input class="form-control" type="text" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input class="form-control" type="text" name="email" id="email">
                </div>
                <div class="form-group">
                    <label for="subject_matter">Assunto:</label>
                    <input class="form-control" type="text" name="subject_matter" id="subject_matter">
                </div>
                <div class="form-group">
                    <label for="message">Mensagem:</label>
                    <textarea class="form-control" name="message" id="message" rows="5"></textarea>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection