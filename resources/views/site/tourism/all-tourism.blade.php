@extends('site.template.layout')

@section('title', 'Todos Locais de Turismo')

@section('content')

    <div class="site-tourism-all-tourism-title container-fluid">
        <div class="row col-md-12">
                <h1>Todos os Turismo</h1>
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Pesquisar</button>
                </form>
        </div>
    </div>

    <div class="site-tourism-all-tourism container mt-5">
        <div class="row">
            @foreach($tourisms as $tourism)
                <div class="col-md-4">
                    <a href="{{ route('site.tourism.id', $tourism->id) }}">
                        <div class="news-all-news-image-main-event card">
                            <div class="card-header">
                                <h3>{{ substr($tourism->name, 0, 60) }}</h3>
                            </div>
                            <div class="card-body">
                                <img class="img-responsivo" src="{{ asset('/storage/tourism/'.$tourism->main_image) }}" alt="Saltinho">
                            </div>
                            <div class="card-footer">
                                <h5>Clique pra ver mais detalhes!</h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            <div class="col-md-12">
                @if($tourisms)
                    <div style="margin: 5px;">
                        {{ $tourisms->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
