@extends('site.template.layout')

@section('title', 'Turísmo')

@section('content')

@section('head-script')
<script src="{{ asset('js/site/pages/tourism.js')}}"></script>
@endsection

<div class="container">
    <div class="row">
        <div class="tourism-selected col-md-12">
            <div class="card">
                <div class="tourism-selected-card">
                    <div class="col-md-12">
                        <h3>{{ $tourism->name }}</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="img-thumbnail" src="{{asset('/imagens/satinho.jpg')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6 mt-2">
                                    <strong>Conhecido por:</strong> {{ $tourism->name }}
                                </div>
                                <div class="col-md-6 mt-2">
                                    <strong>Localizado:</strong> <a href="https://www.google.com.br/maps/place/Parque+Ecologico+Henrique+Barbeta/@-22.4300776,-47.7286405,17z/data=!3m1!4b1!4m5!3m4!1s0x94c7c7272406a239:0x6a4378d6ef16747c!8m2!3d-22.4300776!4d-47.7264465">ver localização</a>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <strong>Contem:</strong> {{ $tourism->contains }}
                                </div>
                                <div class="col-md-6 mt-2">
                                    <strong>Paga alguma taxa:</strong> {{ $tourism->pay === 1 ? 'Sim' : 'Não' }}
                                </div>
                                <div class="col-md-12 mt-2">
                                    <strong>Descrição:</strong>
                                    <p>{{ $tourism->description }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-md-12">
                        <h3>Clique nas Imagens Para Ampliar</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div class="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow2" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Trigger the Modal -->
                            <img class="myImgTourism" src="{{ asset('/imagens/ipe.jpg') }}" alt="Snow" style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- The Close Button -->
                                <span class="close">&times;</span>

                                <!-- Modal Content (The Image) -->
                                <img class="modal-content" id="img01">

                                <!-- Modal Caption (Image Text) -->
                                <div id="caption"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
