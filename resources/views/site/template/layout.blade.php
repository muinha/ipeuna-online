<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/site/style.css">
    <link rel="stylesheet" href="/css/site/style-mobile.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-social.css" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <!-- validate css -->
    <link rel="stylesheet" href="/css/validate.css">
    <link rel="stylesheet" href="/assets/libs/sweetalert2/dist/sweetalert2.min.css">

    <!-- toastr -->
    <link rel="stylesheet" href="/assets/libs/toastr/build/toastr.min.css">
    <script src="/assets/libs/toastr/build/toastr.min.js"></script>

    @yield('head-script')
</head>

<body style="background: #dbdbdb;">

  <div id="app"></div>

  <!-- Início Menu de Navegação -->

    <div class="menu-navegation-top container-fluid">
      <div id="topo_account" class="container">
        <nav class="navbar navbar-expand-lg">
          <ul class="navbar-nav ml-auto">
            @auth                
            <li class="nav-item">
              <a class="nav-link-account" href="{{ Auth::user()->system_profile_id !== 2 ? route('admin.index') : route('site.admin.index') }}"><i class="fa fa-user-circle"></i> Minha Conta</a>
              <a style="margin-left: 10px;" class="nav-link-account" href="{{ Auth::user()->system_profile_id !== 2 ? route('admin.logout') : route('site.logout') }}"><i class="fa fa-power-off"></i> Sair</a>
            </li>
            @else
            <li class="nav-item text-white">
              <a class="nav-link-account" href="{{ route('site.show.login') }}"><i class="fa fa-lock"></i> Login</a> 
              <a style="margin-left: 10px;" class="nav-link-account" href="{{ route('site.show.register') }}"><i class="fa fa-user-plus"></i> Registrar</a>
            </li>
            @endif
          </ul>
        </nav>
      </div>
      <div class="container">
        <div class="col-md-12">
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark">
          <a class="h1-title" href="{{ route('site.index') }}">Ipeúna Online</a>
          <button style="colo:white;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#textoNavbar" aria-controls="textoNavbar" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="textoNavbar">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('site.index') }}">Início</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('site.all.tourism') }}">Turismo</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('site.all.news') }}">Notícias</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Utílidades
                </a>
              <div style="background-color: #1a263d;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="{{ route('site.utility.bus.time') }}">Horários de Ônibus </a>
                <a class="dropdown-item" href="#">Segunda Utílidade</a>
                <a class="dropdown-item" href="#">Terceira Utílidade</a>
              </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('site.contact') }}">Contato</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    <!-- Início Conteúdo do Site -->
    @yield('content')


      <div class="footer-template container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 text-center social-network">
                    <h1>Rede Social</h1>
                    <a class="btn btn-social-icon btn-facebook" href="#">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a class="btn btn-social-icon btn-twitter" href="#">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a class="btn btn-social-icon btn-google" href="#">
                        <span class="fa fa-google"></span>
                    </a>
                    <hr>
                    <h1>Endereço:</h1>
                    <p>Avenida: Ercidio morato</p>
                    <p>N: 352</p>
                    <p>Altos de Ipeúna</p>
                </div>
                <div class="col-md-4 col-sm-12 text-center utility-footer">
                    <h1>Categorias</h1>
                    <ul class="list-group">
                        <a href="">
                            <li class="list-group-item">Categoria 1</li>
                        </a>
                        <a href="">
                            <li class="list-group-item">Categoria 2</li>
                        </a>
                        <a href="">
                            <li class="list-group-item">Categoria 3</li>
                        </a>
                        <a href="">
                            <li class="list-group-item">Categoria 4</li>
                        </a>
                        <a href="">
                            <li class="list-group-item">Categoria 5</li>
                        </a>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-12 text-center social-network">
                    <p style="text-align: center; font-family: 'Ubuntu', sans-serif; color: #FFF0F5;">&reg; Copyright 2019 By <a style="font-weight: bold;" class="nav-link" href="">Samuel Sanches</a> </p>
                    <h3>Rede Social</h3>
                    <a class="btn btn-social-icon btn-facebook" href="#">
                        <span class="fa fa-facebook"></span>
                    </a>
                    <a class="btn btn-social-icon btn-twitter" href="#">
                        <span class="fa fa-twitter"></span>
                    </a>
                    <a class="btn btn-social-icon btn-google" href="#">
                        <span class="fa fa-google"></span>
                    </a>
                    <hr>
                </div>
            </div>
        </div>
    </div>


    @yield('footer-script')
    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>

        <script src="/js/admin/pages/users/users.js"></script>

        <!-- All Js -->
        <script src="/js/jquery.mask.js"></script>
        <script src="/js/utility.js"></script>
        <script src="/js/jquery.isloading.js"></script>
        <!-- End All Js --> 
        
        <!-- Validate Jquery-->
        <script src="/assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
        <!-- End validate-->

</body>

</html>
