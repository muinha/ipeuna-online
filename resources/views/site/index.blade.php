@extends('site.template.layout')

@section('title', 'Ipeúna Online')

@section('content')

<header>
    <div id="img_index_main">
        <img src="/imagens/ipe.jpg" alt="principal">
    </div>
</header>

<div class="container-fluid">
    <div class="title-card-news-index">
        <h2>Notícias Recentes</h2>
    </div>
    <div class="row">
        @forelse($news as $noticia)
            <div class="col-md-3">
                <div class="card-news">
                    <a href="{{ route('site.news.id', $noticia->id) }}">
                    <img class="card-news-img-top" src="{{ asset('/storage/news/'.$noticia->main_image) }}" alt="Card-news image cap">
                        <div class="card-news-body">
                            <h5 class="card-news-title">{{ substr($noticia->title, 0, 80) }}</h5>
                        </div>
                    </a>
                </div>
            </div>
        @empty
            <div class="container mt-4 mb-4">
                <div class="col-12">
                    <p>Não a registros</p>
                </div>
            </div>
        @endforelse
    </div>
</div>
<div class="container-fluid">
    <div class="title-card-tourism-index">
        <h2>Conheça Área Turísmo</h2>
    </div>
    <div class="row">
        @forelse($tourism as $turismo)
            <div class="col-md-4">
                <div style="background: rgba(255, 255, 255, 0.548);" class="card-tourism">
                    <a href="{{ route('site.tourism.id', $turismo->id) }}">
                    <img class="card-tourism-img-top" src="{{ asset('/storage/tourism/'.$turismo->main_image) }}" alt="Card-tourism image cap">
                        <div class="card-tourism-body">
                            <h5 class="card-tourism-title">{{ substr($turismo->name, 0, 55) }}</h5>
                            <p class="card-tourism-text">"{{ substr($turismo->description, 0, 110) }}</p>
                        </div>
                    </a>
                </div>
            </div>
        @empty
            <div class="container mt-4 mb-4">
                <div class="col-12">
                    <p>Não a registros</p>
                </div>
            </div>
        @endforelse
    </div>
</div>


@endsection
