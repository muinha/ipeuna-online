<li class="sidebar-item">
    <a class="sidebar-link has-arrow waves-effect waves-dark profile-dd" href="{{ route('site.user.profile', Auth::user()->id) }}" aria-expanded="false">
        <img src="{{ Auth::user()->image_user ? asset('/storage/users/'.Auth::user()->image_user)  : '/imagens/user-perfil-padrao.jpeg' }}" class="rounded-circle ml-2" width="30">
        <span class="hide-menu">{{ Auth::user()->name }} </span>
    </a>
    <ul aria-expanded="false" class="collapse  first-level">
        <li class="sidebar-item">
            <a href="{{ route('site.user.profile', Auth::user()->id) }}" class="sidebar-link">
                <i class="ti-user"></i>
                <span class="hide-menu"> Meu Perfil </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('site.admin.user.profile.edit', Auth::user()->id) }}" class="sidebar-link">
                <i class="mdi mdi-account-edit"></i>
                <span class="hide-menu"> Editar Perfil </span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('site.logout') }}" class="sidebar-link">
                <i class="fas fa-power-off"></i>
                <span class="hide-menu"> Sair </span>
            </a>
        </li>
    </ul>
</li>

<li class="sidebar-item">
    <a href="{{ route('site.admin.index') }}" class="sidebar-link">
        <i class="fas fa-home"></i>
        <span class="hide-menu"> Dashboard </span>
    </a>
</li>