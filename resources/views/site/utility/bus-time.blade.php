@extends('site.template.layout')

@section('section', 'Horários de Ônibus')

@section('head-script')
<script src="{{ asset('js/pages/site/utility/bus-time.js')}}"></script>
@endsection

@section('content')

    <div class="site-utility-bus-time-title container-fluid">
        <h1>Horários de Ônibus</h1>
    </div>

    <div class="site-utility-bus-time container">
        <div class="card col-md-12">
            <h2>Ipeúna a Rio Claro</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>Saída</th>
                            <th class="text-center">Domingo</th>
                            <th class="text-center">Segunda</th>
                            <th class="text-center">Terça</th>
                            <th class="text-center">Quarta</th>
                            <th class="text-center">Quinta</th>
                            <th class="text-center">Sexta</th>
                            <th class="text-center">Sábado</th>
                            <th class="text-center">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>06:10</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>07:00</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>08:00</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>10:00</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>13:00</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>14:00</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>15:30</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>17:30</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>18:20</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>22:10</th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card col-md-12">
            <h2>Rio Claro a Ipeúna</h2>
            <div class="table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Saída</th>
                        <th class="text-center">Domingo</th>
                        <th class="text-center">Segunda</th>
                        <th class="text-center">Terça</th>
                        <th class="text-center">Quarta</th>
                        <th class="text-center">Quinta</th>
                        <th class="text-center">Sexta</th>
                        <th class="text-center">Sábado</th>
                        <th class="text-center">Preço</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>06:10</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>09:00</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>11:00</th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>12:10</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>12:40</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>15:00</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>16:00</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>17:15</th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>18:20</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                    <tr>
                        <th>22:45</th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                        <th class="text-center bg-danger"><i style="color: white;" class="fa fa-times"></i></th>
                        <th class="text-center">R$ 6,19</th>   
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
        <div class="card col-md-12">
            <h2>Charqueada a Ipeúna</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>Saída</th>
                            <th class="text-center">Domingo</th>
                            <th class="text-center">Segunda</th>
                            <th class="text-center">Terça</th>
                            <th class="text-center">Quarta</th>
                            <th class="text-center">Quinta</th>
                            <th class="text-center">Sexta</th>
                            <th class="text-center">Sábado</th>
                            <th class="text-center">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>07:30</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>15:00</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card col-md-12">
            <h2>Ipeúna a Charqueada</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>Saída</th>
                            <th class="text-center">Domingo</th>
                            <th class="text-center">Segunda</th>
                            <th class="text-center">Terça</th>
                            <th class="text-center">Quarta</th>
                            <th class="text-center">Quinta</th>
                            <th class="text-center">Sexta</th>
                            <th class="text-center">Sábado</th>
                            <th class="text-center">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>11:45</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                        <tr>
                            <th>18:00</th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center bg-success"><i style="color: white;" class="fa fa-check"></i></th>
                            <th class="text-center">R$ 6,19</th>   
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
