@extends('site.template.layout')

@section('title', 'Todas as Notícias')

@section('content')

@section('head-script')
    <script src=""></script>
@endsection

    <div class="news-all-title container-fluid">
        <h2>Notícias Em Destaques</h2>
    </div>
    <div class="news-all-news-image-main container">
        <div class="row">
            @foreach($newsMain as $news)
            <div class="news-all-news-image-main-event col-md-6">
                <a href="{{ route('site.news.id', $news->id) }}">
                    <h4>{{ \App\Models\NewsCategory::getCategoryName($news->category) }}</h4>
                    <img src="{{ asset('/storage/news/'.$news->main_image) }}" alt="Notícia Principal">
                    <h3 class="text-center">{{ substr($news->title, 0, 70) }}</h3>
                </a>
            </div>
            @endforeach
        </div>
    </div>

    <div class="news-all-title container-fluid">
        <h2>Ultímas Notícias Adicionadas</h2>
    </div>
    <div class="news-all-news-seccondary container">
        <div class="row">
            <div style="margin-left: 15px;" class="col-12">
                <form action="">
                    <div class="row">
                        <div class="col-md-10">
                            <input type="search" name="search" id="search" class="pull-right form-control" placeholder="Pesquisar...">
                        </div>
                        <div class="col-md-2">
                            <input type="submit" value="Pesquisar" class="btn btn-success">
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-12">
                @foreach($allNews as $news)
                    <div class="news-all-news-image-main-event card">
                        <a href="{{ route('site.news.id', $news->id) }}">
                            <img src="{{ asset('/storage/news/'.$news->main_image) }}" alt="Notícia Secundaria">
                            <h5><strong>{{ \App\Models\NewsCategory::getCategoryName($news->category) }}</strong> - {{ $news->created_at }}</h5>
                            <h3>{{ substr($news->title, 0, 70) }}</h3>
                        </a>
                    </div>
                @endforeach
                @if($allNews)
                    <div style="margin: 5px;">
                        {{ $allNews->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="news-all-news-third container">
        <div class="row">
            <div class="news-all-news-card-one col-md-8">
                <div class="news-all-title container-fluid">
                    <h2>Notícias Mais Acessadas</h2>
                </div>
                @foreach($newsQtd as $news)
                <div class="news-all-news-image-main-event card">
                    <a href="{{ route('site.news.id', $news->id) }}">
                        <img src="{{ asset('/storage/news/'.$news->main_image) }}" alt="Notícia Secundaria">
                        <h5><strong>{{ \App\Models\NewsCategory::getCategoryName($news->category) }}</strong> - {{ $news->created_at }}</h5>
                        <h3>{{ substr($news->title, 0, 110) }}</h3>
                    </a>
                </div>
                <hr>
                @endforeach
                @if($allNews)
                    <div style="margin: 5px;">
                        {{ $allNews->links() }}
                    </div>
                @endif
            </div>
            <div class="news-all-news-card-two col-md-4">
                <div class="card">
                    <div style="background: green;" class="card-header">
                        <h5>{{ \App\Models\NewsCategory::getCategoryName($newsSport->category) }}</h5>
                    </div>
                    <div class="news-all-news-image-main-event card-body">
                        <a href="{{ route('site.news.id', $news->id) }}">
                            <img src="{{ asset('/storage/news/'.$newsSport->main_image) }}" alt="Notícia Secundaria">
                            <p>{{ substr($newsSport->title, 0, 78) }}</p>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div style="background: #801419;" class="card-header">
                        <h5>{{ \App\Models\NewsCategory::getCategoryName($newsGov->category) }}</h5>
                    </div>
                    <div class="news-all-news-image-main-event card-body">
                        <a href="{{ route('site.news.id', $news->id) }}">
                            <img src="{{ asset('/storage/news/'.$newsGov->main_image) }}" alt="Notícia Secundaria">
                            <p>{{ substr($newsGov->title, 0, 78) }}</p>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div style="background: chocolate;" class="card-header">
                        <h5 >{{ \App\Models\NewsCategory::getCategoryName($newsEntertainment->category) }}</h5>
                    </div>
                    <div class="news-all-news-image-main-event card-body">
                        <a href="{{ route('site.news.id', $news->id) }}">
                            <img src="{{ asset('/storage/news/'.$newsEntertainment->main_image) }}" alt="Notícia Secundaria">
                            <p>{{ substr($newsEntertainment->title, 0, 78) }}</p>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div style="background: #801419;" class="card-header">
                        <h5>{{ \App\Models\NewsCategory::getCategoryName($newsPolice->category) }}</h5>
                    </div>
                    <div class="news-all-news-image-main-event card-body">
                        <a href="{{ route('site.news.id', $news->id) }}">
                            <img src="{{ asset('/storage/news/'.$newsPolice->main_image) }}" alt="Notícia Secundaria">
                            <p>{{ substr($newsPolice->title, 0, 78) }}</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

