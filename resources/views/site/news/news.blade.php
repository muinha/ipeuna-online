@extends('site.template.layout')

@section('title', 'Notícias')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="news-selected card">
                <div class="card-header">
                    <h1>{{ $news->title }}</h1>
                    <div class="card-body">
                        <h2>{{ $news->subtitle }}</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- span one computador -->
                                <span id="news-selected-span-one">13 de agosto de 2019 as 16:02 - {{ $news->name }}</span>
                                <!-- span two celular -->
                                <span id="news-selected-span-two">{{ $news->created_at }} - {{ $news->name }}</span>
                                <!-- facebook plugin -->
                                <div id="news-selected-facebook" class="fb-share-button"
                                    data-href="http://ipeuna.local/noticia"
                                    data-layout="button_count">
                                </div>
                            </div>
                        </div>
                        <img class="img-thumbnail" src="{{ asset('/storage/news/'.$news->main_image) }}" alt="news">
                        <div class="news-selected-text">
                            {!! $news->news !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

@endsection
