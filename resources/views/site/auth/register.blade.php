@extends('site.template.layout')

@section('title', 'Registro')

@section('content')

<div class="site-register-form container">
    <div class="row">
        <div class="col-md-12">
            @include('messages')
        </div>
        <div class="col-md-12">
            <h1>Formulario de Cadastro</h1>
        </div>
        <div class="col-md-12">
            <form id="form_site_create_user" action="{{ route('site.register') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <h3>Dados da Conta</h3>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="name">Nome</label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Digite seu nome">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" name="email" id="email" placeholder="Digite seu email">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="username">Usuário</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="Digite seu usuário">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="password">Senha</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="Digite sua senha">
                    </div>
                    <div class="col-md-12">
                        <h3>Dados Pessoais</h3>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="cep">CEP</label>
                        <input class="form-control" type="text" name="cep" id="cep" placeholder="00000-000">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="city">Cidade</label>
                        <input class="form-control" type="text" name="city" id="city" placeholder="Digite sua cidade">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="states">Estado</label>
                        <input class="form-control" type="text" name="states" id="states" placeholder="Digite sua estado">
                    </div>
                    <div class="col-md-8 form-group">
                        <label for="address">Endereço</label>
                        <input class="form-control" type="text" name="address" id="address" placeholder="Digite sua endereço">
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="phone">Telefone</label>
                        <input class="form-control" type="text" name="phone" id="phone" placeholder="(00)00000-0000">
                    </div>
                    <div class="col-md-12 form-group">
                        <h3>Adicione uma descrição para seu perfil</h3>
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="description">Descrição</label>
                        <textarea placeholder="Adicione uma descrição" class="form-control" name="description" id="description" rows="10"></textarea>
                    </div>
                    <div class="col-md-12">
                        <input class="btn btn-success btn-lg btn-block" type="submit" value="Cadastrar">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('footer-script')
    <script src="/js/site/pages/user/user.js"></script>
@endsection