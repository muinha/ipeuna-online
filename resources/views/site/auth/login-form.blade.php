@extends('site.template.layout')

@section('title', 'Login')
    
@section('content')
    
    <div class="site-form-login-title container-fluid">
        <h1>Formulario de Login</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('messages')
            </div>
            <div class="site-form-login-desc col-md-5">
                <h3>Seja bem vindo !!</h3>
                <h5 class="mt-2">Não é usuário do site ainda?</h5>
                <a class="nav-link" href="{{ route('site.show.register') }}"><h5>Clique aqui para se cadastrar</h5></a>
                <p>Usuários cadastrados podem aproveitar bem mais o site, aproveite e entre, ou se registre</p>
            </div>
            <div class="site-form-login col-md-6">
                <form id="site_login_form_admin" action="{{ route('site.login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="username">Usuário</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="Digite seu usuário">
                    </div>
                    <div class="form-group">
                        <label for="password">Senha</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="Digite sua senha">
                    </div>
                    <p>
                        <span><a href=""><i class="fa fa-lock"></i> Esqueceu a senha</a></span>
                        <span style="margin-left: 10px;"><a href="{{ route('site.show.register') }}"><i class="fa fa-user-plus"></i> Cadastre-se</a></span>
                    </p>
                    <input class="btn btn-success btn-lg btn-block" type="submit" value="Entrar">
                </form>
                <a class="btn btn-warning btn-lg btn-block mt-2" href="{{ route('site.show.register') }}"> Cadastre-se</a>
                <a class="btn btn-facebook btn-lg btn-block" href="">Entrar com Facebook</a>
                <a class="btn btn-google btn-lg btn-block mb-3" href="">Entrar com Google</a>
            </div>
        </div>
    </div>

@endsection

@section('footer-script')
    <script src="/js/site/pages/user/user.js"></script>
@endsection