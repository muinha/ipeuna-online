@extends('admin.template.template')

@section('title',  $user['id'] ? 'Editar Perfil' : 'Criar Usuário' )

@section('breadcrumb')

    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
            <h5 class="font-medium text-uppercase mb-0 text-center">{{ $user['id'] ? 'Editar Perfil' : 'Criar Usuário' }}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $user['id'] ? 'Editar Perfil' : 'Criar Usuário' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    
@endsection

@section('content')
    @include('messages')
    <div class="card">
        <form id="form_admin_create_users" enctype="multipart/form-data" method="POST" action="{{ !$user->id ? route('admin.users.save') : route('admin.user.profile.update', $user['id']) }}" class="form pt-3">
            @csrf
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="el-element-overlay">
                        <div class="el-card-item">
                            <img style="height: 400px;" class="img-thumbnail" src="{{ $user['image_user'] ? asset('/storage/users/'.$user->image_user)  : '/imagens/user-perfil-padrao.jpeg' }}" alt="user">
                        </div>
                    </div>
                    <div style="width: 100%;" class="btn btn-info waves-effect waves-light">
                        <span>Carregar Imagem Perfil</span>
                        <input value="{{ $user->image_user }}" name="image_user" id="image_user" type="file" class="upload"> 
                    </div>
                </div>
                <div class="col-md-6">
                    <ul class="nav nav-pills custom-pills mb-4" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-profile-tab-account" data-toggle="pill" href="#tabs_users_data_account" role="tab" aria-controls="pills-timeline" aria-selected="true">Dados da Conta</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab-personal" data-toggle="pill" href="#tabs_users_data_personal" role="tab" aria-controls="pills-profile" aria-selected="false">Dados Pessoais</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab-description" data-toggle="pill" href="#tabs_users_data_description" role="tab" aria-controls="pills-profile" aria-selected="false">Descrição Perfil</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade active show" id="tabs_users_data_account" role="tabpanel" aria-labelledby="pills-profile-tab-account">
                            <div class="form-group">
                                <label for="name">Nome</label>
                                <input value="{{ $user['name'] ? $user['name'] : '' }}"  id="name" name="name" type="text" class="form-control" placeholder="Digite seu nome">
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input value="{{ $user['email'] ? $user['email'] : '' }}" id="email" name="email" type="email" class="form-control" placeholder="Digite seu e-mail">
                            </div>
                            @if (Auth::user()->system_profile_id === 1)
                                <div class="form-group">
                                    <label for="system_profile">Perfil do Sístema</label>
                                    <select class="form-control" name="system_profile_id" id="system_profile">
                                        @foreach (\App\Models\SystemProfile::getAllProfile() as $profile)
                                            <option value="{{ $profile->id }}" {{ $user->system_profile_id == $profile->id ? 'selected' : '' }}>{{ $profile->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="username">Usuário</label>
                                <input value="{{ $user['username'] ? $user['username'] : '' }}" id="username" name="username" type="text" class="form-control" placeholder="Digite seu usuário">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input value="{{ $user['password'] ? $user['password'] : '' }}"   id="password" name="password" type="password" class="form-control" placeholder="Digite seu password">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs_users_data_personal" role="tabpanel" aria-labelledby="pills-profile-tab-personal">
                            <div class="form-group">
                                <label for="cep">Cep</label>
                                <input value="{{ $user['cep'] ? $user['cep'] : '' }}"  id="cep" name="cep" type="text" class="form-control" placeholder="Digite seu Cep">
                            </div>
                            <div class="form-group">
                                <label for="city">Cidade</label>
                                <input value="{{ $user['city'] ? $user['city'] : '' }}"  id="city" name="city" type="text" class="form-control" placeholder="Digite sua Cidade">
                            </div>
                            <div class="form-group">
                                <label for="states">Estado</label>
                                <input value="{{ $user['states'] ? $user['states'] : '' }}" id="states" name="states" type="text" class="form-control" placeholder="Digite seu estado">
                            </div>
                            <div class="form-group">
                                <label for="address">Endereço</label>
                                <input value="{{ $user['address'] ? $user['address'] : '' }}" id="address" name="address" type="text" class="form-control" placeholder="Digite seu endereço">
                            </div>
                            <div class="form-group">
                                <label for="phone">Telefone</label>
                                <input value="{{ $user['phone'] ? $user['phone'] : '' }}" id="phone" name="phone" type="text" class="form-control" placeholder="Digite seu endereço">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs_users_data_description" role="tabpanel" aria-labelledby="pills-profile-tab-description">
                            <div class="form-group">
                                <Label for="description">Descrição</Label>
                                <textarea class="form-control" name="description" id="description" cols="30" rows="10" placeholder="Adicione uma descrição para seu perfil">{{ $user['description'] ? $user['description'] : '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <a href="{{ $user->id ? route('admin.index') : route('admin.users.list') }}" class="btn btn-dark">Cancelar</a>
                        <button type="submit" class="btn btn-success mr-2">{{ $user->id ? 'Atualizar' : 'Cadastrar' }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection