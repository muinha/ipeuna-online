@extends('admin.template.template')

@section('title',  $tourism['id'] ? 'Editar Turismo' : 'Criar Turismo' )

@section('breadcrumb')

    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
            <h5 class="font-medium text-uppercase mb-0 text-center">{{ $tourism['id'] ? 'Editar Turismo' : 'Criar Turismo' }}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.tourism.list') }}">Lista Turismos</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $tourism['id'] ? 'Editar Turismo' : 'Criar Turismo' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')
    @include('messages')
        <form id="form_admin_create_tourism" enctype="multipart/form-data" method="POST" action="{{ !$tourism->id ? route('admin.tourism.save') : route('admin.tourism.update', $tourism['id']) }}" class="form pt-3">
            @csrf
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="el-element-overlay">
                        <div class="el-card-item">
                            <img style="height: 400px;" class="img-thumbnail" src="{{ $tourism['main_image'] ? asset('/storage/tourism/'.$tourism->main_image)  : '/imagens/notfound.png' }}" alt="user">
                        </div>
                    </div>
                    <div style="width: 100%;" class="btn btn-info waves-effect waves-light">
                        <span>Imagem Principal</span>
                        <input name="main_image" id="main_image" type="file" class="upload">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="name">Nome</label>
                            <input value="{{ $tourism->name ? $tourism->name : '' }}" class="form-control" name="name" id="name" placeholder="Adicione um Nome">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="localization">Localização</label>
                            <input value="{{ $tourism->localization ? $tourism->localization : '' }}" class="form-control" name="localization" id="localization" placeholder="Adicione um localização">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="category">Categoria</label>
                            <select class="form-control" name="category" id="category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->id === $tourism->category ? 'selected' : '' }}>{{ $category->category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="pay">Pago</label>
                            <select class="form-control" name="pay" id="pay">
                                <option {{ $tourism->pay == 1 ? 'selected' : '' }} value="1">Sim</option>
                                <option {{ $tourism->pay == 0 ? 'selected' : '' }} value="0">Não</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="contains">Contem</label>
                            <textarea name="contains" id="contains" cols="30" rows="7" class="form-control">{{ $tourism->contains ? $tourism->contains : '' }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Descrição</label>
                        <textarea class="ckeditor" name="description" id="description" cols="30" rows="15" placeholder="Adicione a descrição">{{ $tourism->description ? $tourism->description : '' }}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <a href="{{ route('admin.tourism.list') }}" class="btn btn-secondary btn-lg">Voltar</a>
                    <input style="margin-left: 10px;" class="btn btn-success btn-lg" type="submit" value="{{ $tourism->id ? 'Editar' : 'Cadastrar' }}">
                    @if($tourism->id)
                        <a class="btn btn-danger btn-lg pull-right" href="{{ route('admin.tourism.delete', $tourism->id) }}">Deletar Descrição</a>
                    @endif
                </div>
            </div>
        </form>

@endsection
