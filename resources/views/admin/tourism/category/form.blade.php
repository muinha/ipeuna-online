@extends('admin.template.template')

@section('title',  $category['id'] ? 'Editar Turismo' : 'Criar Turismo' )

@section('breadcrumb')

    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
            <h5 class="font-medium text-uppercase mb-0 text-center">{{ $category['id'] ? 'Editar Turismo' : 'Criar Turismo' }}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.tourism.category.list') }}">Lista Turismos</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $category['id'] ? 'Editar Turismo' : 'Criar Turismo' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    
@endsection

@section('content')
    @include('messages')
        <form id="form_admin_create_category" enctype="multipart/form-data" method="POST" action="{{ !$category->id ? route('admin.tourism.category.save') : route('admin.tourism.category.update', $category['id']) }}" class="form pt-3">
            @csrf
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="category">Categoria</label>
                    <input value="{{ $category->category ? $category->category : '' }}" class="form-control" type="text" name="category" id="category" placeholder="Digite a nova categoria de turismo">
                </div>
                <div class="col-md-12">
                    <a href="{{ route('admin.tourism.category.list') }}" class="btn btn-secondary btn-lg">Voltar</a>
                    <input style="margin-left: 10px;" class="btn btn-success btn-lg" type="submit" value="{{ $category->id ? 'Editar' : 'Cadastrar' }}">
                    @if($category->id)
                        <a class="btn btn-danger btn-lg pull-right" href="{{ route('admin.tourism.category.delete', $category->id) }}">Deletar Descrição</a>
                    @endif
                </div>
            </div>
        </form>

@endsection