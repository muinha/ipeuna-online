@extends('admin.template.template')

@section('title', 'Perfis de Sistemas')

@section('breadcrumb')
    
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Perfis de Sistemas</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Perfis de Sistemas</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')

<!-- ============================================================== -->
    <!-- basic table -->
    <div class="row">
        <div class="col-md-6">
            @include('messages')
        </div>
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div style="margin: 5px 0px 35px 0px;">
                        <a href="{{ route('admin.system.profile.create') }}" class="btn btn-success btn-lg text-white">Criar Perfil<i style="margin-left: 10px;" class="fa fa-plus"></i> </a>
                    </div>
                    <div class="table-responsive">
                        <table id="table_system_profile" class="table table-striped border">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th class="text-center">Perfil</th>
                                    <th class="text-center">Ações</th>
                            </thead>
                            <tbody>
                                @foreach(\App\Models\SystemProfile::getAllProfile() as $profile)
                                    <tr>
                                        <td>{{ $profile->id }}</td>
                                        <td class="text-center">{{ $profile->name }}</td>
                                        <td  class="text-center">
                                            <a href="{{ route('admin.system.profile.edit', $profile->id) }}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                            <a href="{{ route('admin.system.profile.delete', $profile->id) }}" class="btn btn-danger btn-circle btn-delete"><i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script-footer')
    <script src="/js/admin/pages/system-profile/system-profile.js"></script>
@endsection