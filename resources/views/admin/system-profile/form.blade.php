@extends('admin.template.template')

@section('title', $profiles->id ? 'Editar Perfil' : 'Criar Perfil')

@section('breadcrumb')
    
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Perfis de Sistemas</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.system.profile.list') }}">Perfis de Sistemas</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $profiles->id ? 'Editar Perfil' : 'Criar Perfil' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')

    <div class="card">
        @include('messages')
        <form id="form_system_profile" action="{{ $profiles->id ? route('admin.system.profile.update', $profiles->id) : route('admin.system.profile.save') }}" method="post">
            @csrf
            <div class="row p-5">
                <div class="col-md-10 form-group">
                    <label for="system_profile_name label">Nome do novo perfil</label>
                    <input class="form-control" value="{{ $profiles->name ? $profiles->name : '' }}" type="text" name="name" id="system_profile_name" placeholder="Nome do novo perfil">
                </div>
                <div style="margin:auto;" class="col-md-2 form-group">
                    <a class="btn btn-danger" href="{{ route('admin.system.profile.list') }}">Voltar</a>
                    <input class="btn btn-success" type="submit" value="{{ $profiles->id ? 'Atualizar' : 'Cadastrar' }}">
                </div>
            </div>
        </form>
    </div>

@endsection


@section('script-footer')
    <script src="/js/admin/pages/system-profile/system-profile.js"></script>
@endsection