@if(Auth::user()->system_profile_id !== 2)
    <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark profile-dd" href="{{ route('site.user.profile', Auth::user()->id) }}" aria-expanded="false">
            <img src="{{ Auth::user()->image_user ? asset('/storage/users/'.Auth::user()->image_user)  : '/imagens/user-perfil-padrao.jpeg' }}" class="rounded-circle ml-2" width="30">
            <span class="hide-menu">{{ Auth::user()->name }} </span>
        </a>
        <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item">
                <a href="{{ route('site.user.profile', Auth::user()->id) }}" class="sidebar-link">
                    <i class="ti-user"></i>
                    <span class="hide-menu"> Meu Perfil </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.user.profile.edit', Auth::user()->id) }}" class="sidebar-link">
                    <i class="mdi mdi-account-edit"></i>
                    <span class="hide-menu"> Editar Perfil </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.logout') }}" class="sidebar-link">
                    <i class="fas fa-power-off"></i>
                    <span class="hide-menu"> Sair </span>
                </a>
            </li>
        </ul>
    </li>

    <li class="sidebar-item">
        <a href="{{ route('admin.index') }}" class="sidebar-link">
            <i class="fas fa-home"></i>
            <span class="hide-menu"> Dashboard </span>
        </a>
    </li>

    <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="mdi mdi-account-multiple"></i>
            <span class="hide-menu">Usuários</span>
        </a>
        <ul aria-expanded="false" class="collapse first-level">
            <li class="sidebar-item">
                <a href="{{ route('admin.users.list') }}" class="sidebar-link">
                    <i class="mdi mdi-account-box"></i>
                    <span class="hide-menu"> Lista de Usuários </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.users.create') }}" class="sidebar-link">
                    <i class="mdi mdi-account-plus"></i>
                    <span class="hide-menu"> Criar Usuário </span>
                </a>
            </li>
        </ul>
    </li>

    <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="mdi mdi-apps"></i>
            <span class="hide-menu">Sistemas</span>
        </a>
        <ul aria-expanded="false" class="collapse first-level">
            <li class="sidebar-item">
                <a href="{{ route('admin.system.profile.list') }}" class="sidebar-link">
                    <i class="mdi mdi-account-box"></i>
                    <span class="hide-menu"> Listar Perfis </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.system.profile.create') }}" class="sidebar-link">
                    <i class="mdi mdi-plus"></i>
                    <span class="hide-menu"> Criar Perfil </span>
                </a>
            </li>
        </ul>
    </li>

    <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="far fa-newspaper"></i>
            <span class="hide-menu">Noticias</span>
        </a>
        <ul aria-expanded="false" class="collapse first-level">
            <li class="sidebar-item">
                <a href="{{ route('admin.news.list') }}" class="sidebar-link">
                    <i class="fa fa-newspaper"></i>
                    <span class="hide-menu"> Listar Noticias </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.news.create') }}" class="sidebar-link">
                    <i class="mdi mdi-plus"></i>
                    <span class="hide-menu"> Criar Noticia </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="mdi mdi-apps"></i>
                    <span class="hide-menu">Categorias</span>
                </a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="sidebar-item">
                        <a href="{{ route('admin.news.category.list') }}" class="sidebar-link">
                            <i class="fa fa-align-left"></i>
                            <span class="hide-menu">Listar Categorias</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a href="{{ route('admin.news.category.create') }}" class="sidebar-link">
                            <i class="mdi mdi-plus"></i>
                            <span class="hide-menu">Criar Categoria</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="fas fa-tree"></i>
            <span class="hide-menu">Turismo</span>
        </a>
        <ul aria-expanded="false" class="collapse first-level">
            <li class="sidebar-item">
                <a href="{{ route('admin.tourism.list') }}" class="sidebar-link">
                    <i class="fab fa-pagelines"></i>
                    <span class="hide-menu"> Listar Turismo </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{ route('admin.tourism.create') }}" class="sidebar-link">
                    <i class="mdi mdi-plus"></i>
                    <span class="hide-menu"> Novo Turismo </span>
                </a>
            </li>
            <li class="sidebar-item">
                <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="mdi mdi-apps"></i>
                    <span class="hide-menu">Categorias</span>
                </a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="sidebar-item">
                        <a href="{{ route('admin.tourism.category.list') }}" class="sidebar-link">
                            <i class="fa fa-align-left"></i>
                            <span class="hide-menu">Listas de Categorias</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a href="{{ route('admin.tourism.category.create') }}" class="sidebar-link">
                            <i class="mdi mdi-plus"></i>
                            <span class="hide-menu">Criar Categoria</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

<!-- case user error --> 
@else
    <li class="sidebar-item">
        <a href="{{ route('site.logout') }}" class="sidebar-link">
            <i class="fas fa-power-off"></i>
            <span class="hide-menu"> Sair </span>
        </a>
    </li>
@endif