@extends('admin.template.template')

@section('title', 'Categorias de Noticias')

@section('breadcrumb')
    
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Lista Noticias</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Categorias de Noticias</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')
@include('messages')
<div class="card">
    <div class="col-md-12">
        @include('messages')
    </div>
    <div class="card-body">
        <div style="margin: 5px 0px 35px 0px;">
            <a href="{{ route('admin.news.category.create') }}" class="btn btn-success btn-lg text-white">Criar Categoria<i style="margin-left: 10px;" class="fa fa-plus"></i> </a>
        </div>
        <div class="table-responsive">
            <table id="demo-foo-row-toggler" class="table table-bordered footable footable-1 breakpoint breakpoint-lg" data-toggle-column="first" style="">
                <thead>
                    <tr class="footable-header">           
                        <th>ID</th>
                        <th>Categoria</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody> 
                    @forelse ($category as $cat)
                        <tr>                                
                            <td>{{ $cat->id }}</td>
                            <td>{{ $cat->category }}</td>
                            <td  class="text-center">
                                <a href="{{ route('admin.news.category.edit', $cat->id) }}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                <a href="{{ route('admin.news.category.delete', $cat->id) }}" class="btn btn-danger btn-circle btn-delete"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td>Não tem registro para ser exibido</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(!$category){
            <div style="margin: 5px;">
                {{ $category->links() }}
            </div>
        }
        @endif
    </div>
</div>

@endsection