@extends('admin.template.template')

@section('title',  $category->id ? 'Editar Categoria' : 'Criar Categoria' )

@section('breadcrumb')

    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0 text-center">{{ $category->id ? 'Editar Categoria' : 'Criar Categoria' }}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.news.category.list') }}">Listar Categoria</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $category->id ? 'Editar Categoria' : 'Criar Categoria' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    
@endsection

@section('content')

    @include('messages')
    <form id="form_admin_create_news" enctype="multipart/form-data" method="POST" action="{{ !$category->id ? route('admin.news.category.save') : route('admin.news.category.update', $category->id) }}" class="form pt-3">
        @csrf
        <div class="form-group">
            <label for="category">Categoria</label>
            <input value="{{ $category->category ? $category->category : '' }}" class="form-control" type="text" name="category" id="category" placeholder="Digite a nova categoria de noticias">
        </div>
        <a class="btn btn-secondary" href="{{ route('admin.news.category.list') }}">Voltar</a>
        <input class="btn btn-success" type="submit" value="Cadastrar">
        @if($category->id)
            <a class="btn btn-danger pull-right" href="{{ route('admin.news.category.delete', $category->id) }}">Excluir Categoria</a>
        @endif
    </form>

@endsection