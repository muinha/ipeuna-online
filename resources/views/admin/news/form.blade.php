@extends('admin.template.template')

@section('title',  $noticia['id'] ? 'Editar Noticia' : 'Criar Noticia' )

@section('breadcrumb')

    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
            <h5 class="font-medium text-uppercase mb-0 text-center">{{ $noticia['id'] ? 'Editar Noticia' : 'Criar Noticia' }}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.news.list') }}">Lista Noticias</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $noticia['id'] ? 'Editar Noticia' : 'Criar Noticia' }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')
    @include('messages')
        <form id="form_admin_create_news" enctype="multipart/form-data" method="POST" action="{{ !$noticia->id ? route('admin.news.save') : route('admin.news.update', $noticia['id']) }}" class="form pt-3">
            @csrf
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="el-element-overlay">
                        <div class="el-card-item">
                            <img style="height: 400px;" class="img-thumbnail" src="{{ $noticia['main_image'] ? asset('/storage/news/'.$noticia->main_image)  : '/imagens/notfound.png' }}" alt="user">
                        </div>
                    </div>
                    <div style="width: 100%;" class="btn btn-info waves-effect waves-light">
                        <span>Imagem Principal</span>
                        <input name="main_image" id="main_image" type="file" class="upload">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="title">Titulo</label>
                            <textarea class="form-control" name="title" id="title" cols="30" rows="7" placeholder="Adicione um titulo">{{ $noticia->title ? $noticia->title : '' }}</textarea>
                        </div>
                        <div class="form-group col-12">
                            <label for="subtitle">Sub-Titulo</label>
                            <textarea class="form-control" name="subtitle" id="" cols="30" rows="7" placeholder="Adicione um sub-titulo">{{ $noticia->subtitle ? $noticia->subtitle : '' }}</textarea>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="category">Categoria</label>
                            <select class="form-control" name="category" id="category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div style="margin: 40px auto" class="col-md-4 form-check">
                            <input {{ $noticia->is_main === 1 ? 'checked' : '' }}  value="1" class="form-check-input" type="checkbox" name="is_main" id="is_main">
                            <label class="form-check-label" for="is_main">Definir Como Principal</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="news">Noticia</label>
                        <textarea class="ckeditor" name="news" id="news" cols="30" rows="15" placeholder="Adicione a descrição">{{ $noticia->news ? $noticia->news : '' }}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <a href="{{ route('admin.news.list') }}" class="btn btn-secondary btn-lg">Voltar</a>
                    <input style="margin-left: 10px;" class="btn btn-success btn-lg" type="submit" value="{{ $noticia->id ? 'Editar' : 'Cadastrar' }}">
                    @if($noticia->id)
                        <a class="btn btn-danger btn-lg pull-right" href="{{ route('admin.news.delete', $noticia->id) }}">Deletar Noticia</a>
                    @endif
                </div>
            </div>
        </form>

@endsection
