<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $fillable = [
        'title', 'subtitle', 'category', 'user_id', 'qtd_views', 'news', 'main_image', 'is_main'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

}
