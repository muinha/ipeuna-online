<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tourism extends Model
{

    protected $fillable = [
        'name', 'localization', 'contains', 'pay', 'description', 'avaliation', 'main_image', 'category', 'user_id'
    ];

}
