<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SystemProfile extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public static function getAllProfile()
    {
        return SystemProfile::all();
    } 

    public function user ()
    {
        return $this->hasOne(User::class);
    }
}
