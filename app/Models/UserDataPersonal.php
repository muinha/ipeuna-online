<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDataPersonal extends Model
{
    
    protected $fillable = [
        'user_id' ,'cep', 'city', 'states', 'address', 'phone', 'description'
    ];

    public $timestamps = false;

}
