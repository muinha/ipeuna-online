<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourismCategory extends Model
{
    
    protected $fillable = [
        'category'
    ];

    public $timestamps = false;

}
