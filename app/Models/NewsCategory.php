<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $fillable = [
        'category'
    ];

    public $timestamps = false;

    public static function getCategoryName($id)
    {
        $category = NewsCategory::find($id);

        return $category->category;
    }
}
