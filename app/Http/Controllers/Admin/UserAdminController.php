<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserDataPersonal;
use App\User;
use App\Repository\Admin\UserAdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserAdminController extends Controller
{
    protected $repository;

    public function __construct(UserAdminRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function usersList()
    {
        $users = $this->repository::getAllUsers();

        $totalUsers = $users->count();
        
        return view ('admin.user.list', [
            'users' => $users->paginate(15),
            'totalUsers' => $totalUsers
        ]);

    }

    public function create () 
    {
        $user = new User();

        return view ('admin.user.form', [
            'user' => $user
        ]);
    }

    public function save (Request $request) 
    {     
        $request->validate([
            'email' => 'required|unique:users',
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required'
        ]);
        
        try {
            $this->repository->saveAction($request);

            $request->session()->flash('success', 'Perfil cadastrado com sucesso !!');
            return redirect(route('admin.users.list'));
        } catch (\Exception $e){
            $request->session()->flash('error', 'Erro Verifique as informações abaixo!!');
            return redirect()->back();
        }
        
    }

    public function userProfile($id)
    {

        $user = $this->repository->getUser($id);

        return view ('admin.user.profile', [
            'user' => $user
        ]);

    }
    
    public function userProfileEdit($id)
    {
        $user = $this->repository->getUser($id);
        
        return view ('admin.user.form', [
            'user' => $user
        ]);
    }

    public function userProfileUpdate($id, Request $request)
    { 
        $request->validate([
            'email' => 'required|unique:users,email,'.$id.',id',
            'username' => 'required|unique:users,username,'.$id.',id',
            'name' => 'required',
            'password' => 'required'
        ]);
        
        try {
            $user = $this->repository->updateAction($id, $request);

            $request->session()->flash('success', 'Perfil Editado com sucesso !!');
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Erro ao editar perfil !!');
        }

        return $user->id != Auth::user()->id ? redirect(route('admin.users.list')) : redirect()->back();

    }

    public function delete ($id, Request $request)
    {
        $user = User::find($id);

        if($user->image_user){
            Storage::delete('/users/'.$user->image_user);
        }

        $user->delete();

        if(!$user){
            $request->session()->flash('error', 'Não foi possivel excluir o usuário !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Usuário excluido com sucesso !!');
        return $user->id != Auth::user()->id ? redirect(route('admin.users.list')) : redirect()->back();
    }

}
