<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
    public function index ()
    {
        return view ('admin.auth.login');
    }

    public function authenticate (Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $email = $request->get('email');
        $password = $request->get('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();

            if($user->system_profile_id === 2) {
                Auth::logout();
                $request->session()->flash('error', 'Erro, tente novamente !!');
                return redirect(route('site.login'));
            }

            $user->last_access = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();

            $request->session()->flash('success', 'Você entrou !!');
            return redirect()->intended('admin/dashboard');
        }

        $request->session()->flash('error', 'Email ou senha inválidos!!');
        return redirect()->back();
    }

    public function logout () 
    {
        Auth::logout();
        return redirect(route('admin.login'));
    }

}
