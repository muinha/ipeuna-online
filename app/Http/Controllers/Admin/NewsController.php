<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Repository\Admin\NewsCategoryRepository;
use App\Repository\Admin\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    protected $repository;
    protected $categoryRepository;

    public function __construct(NewsRepository $repository, NewsCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    public function list () 
    {
        $noticias = $this->repository::getAllNews();

        return view ('admin.news.list', [
            'noticias' => $noticias->paginate(10)
        ]);
    }

    public function create () 
    {       
        $noticia = new News();
        $categories = $this->categoryRepository::getAllCategory()
            ->orderBy('id', 'asc')
            ->get();

        return view ('admin.news.form', [
            'noticia' => $noticia,
            'categories' => $categories
        ]);
    }

    public function save (Request $request)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'main_image' => 'required',
            'category' => 'required',
            'news' => 'required'
        ]);

        $news = $this->repository->saveAction($request);

        if(!$news){
            Storage::delete('/news/'.$news->image_user);
            $request->session()->flash('error', 'Erro ao cadastrar a noticia !!');
            return redirect()->back();
        }

        $request->session()->flash('success', "Noticia cadastrada com sucesso !!");
        return redirect(route('admin.news.list'));
    }

    public function edit ($id)
    {
        $noticia = $this->repository->getNews($id);
        $categories = $this->categoryRepository::getAllCategory()
            ->orderBy('id', 'asc')
            ->get();

        return view('admin.news.form', [
            'noticia' => $noticia,
            'categories' => $categories
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'category' => 'required',
            'news' => 'required'
        ]);

        $news = $this->repository->updateAction($request, $id);

        if(!$news){
            Storage::delete('/news/'.$news->main_image);
            $request->session()->flash('error', 'Erro ao editar a noticia !!');
            return redirect()->back();
        }

        $request->session()->flash('success', "Noticia editada com sucesso !!");
        return redirect(route('admin.news.list'));
    }

    public function delete ($id, Request $request)
    {
        $news = News::find($id);

        if($news->main_image){
            Storage::delete('/news/'.$news->main_image);
        }

        $news->delete();

        if(!$news){
            $request->session()->flash('error', 'Não foi possivel excluir a noticia !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Noticia excluida com sucesso !!');
        return redirect(route('admin.news.list'));
    }
}
