<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tourism;
use App\Repository\TourismCategoryRepository;
use App\Repository\TourismRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TourismController extends Controller
{

    protected $repository;
    protected $categoryRepository;

    public function __construct(TourismRepository $repository, TourismCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    public function list ()
    {
        $tourisms = $this->repository::getAllTourism();

        $totTourism = count($tourisms);

        return view ('admin.tourism.list', [
            'tourisms' => $tourisms->paginate(15),
            'totTourism' => $totTourism
        ]);
    }

    public function create ()
    {
        $tourism = new Tourism();
        $categories = $this->categoryRepository::getAllCategory();

        return view ('admin.tourism.form', [
            'tourism' => $tourism,
            'categories' => $categories->get()
        ]);
    }

    public function save (Request $request)
    {
        $request->validate([
            'name' => 'required|unique:tourisms',
            'contains' => 'required',
            'main_image' => 'required',
            'description' => 'required',
        ]);

        $tourism = $this->repository->saveAction($request);

        if (!$tourism) {
            $request->session()->flash('error', 'Erro ao cadastrar turismo !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Turismo cadastrado com sucesso !!');
        return redirect(route('admin.tourism.list'));
    }

    public function edit ($id)
    {
        $categories = $this->categoryRepository::getAllCategory()->get();
        $tourism = $this->repository->getTourism($id)->first();

        return view ('admin.tourism.form', [
            'categories' => $categories,
            'tourism' => $tourism
        ]);
    }

    public function update (Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:tourisms,name,'.$id.',id'
        ]);

        $tourism = $this->repository->updateAction($request, $id);

        if (!$tourism) {
            $request->session()->flash('error', 'Erro ao editar turismo !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Turismo editada com sucesso !!');
        return redirect(route('admin.tourism.list'));
    }

    public function delete (Request $request, $id)
    {
        $tourism = Tourism::find($id);

        if (!$tourism) {
            $request->session()->flash('error', 'Erro, turismo não encontrada !!');
            return redirect()->back();
        }

        if($tourism->main_image){
            Storage::delete('/tourism/'.$tourism->main_image);
        }

        $tourism->delete();

        if (!$tourism) {
            $request->session()->flash('error', 'Erro ao deletar turismo !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Turismo excluido com sucesso !!');
        return redirect(route('admin.tourism.list'));
    }

}
