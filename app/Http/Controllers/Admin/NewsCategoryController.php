<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NewsCategory;
use App\Repository\Admin\NewsCategoryRepository;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    protected $repository;

    public function __construct(NewsCategoryRepository $repository)
    {
        return $this->repository = $repository;
    }

    public function list ()
    {
        $category = $this->repository::getAllCategory();

        return view ('admin.news.category.list', [
            'category' => $category->paginate(15)
        ]);
    }

    public function create ()
    {
        $category = new NewsCategory();

        return view ('admin.news.category.form', [
            'category' => $category
        ]);
    }

    public function save (Request $request)
    {
        $request->validate([
            'category' => 'required|unique:news_categories'
        ]);

        $category = $this->repository->saveAction($request);

        if(!$category){
            $request->session()->flash('error', 'Erro ao cadastrar categoria !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Categoria cadastrada com sucesso !!');
        return redirect(route('admin.news.category.list'));
    }

    public function edit ($id)
    {
        $category = $this->repository::getCategory($id);

        return view ('admin.news.category.form', [
            'category' => $category
        ]);
    }

    public function update (Request $request, $id)
    {
        $request->validate([
            'category' => 'required|unique:news_categories,category,'.$id.',id'
        ]);

        $category = $this->repository->updateAction($request, $id);

        if(!$category){
            $request->session()->flash('error', 'Erro ao editar a categoria !!');
            return redirect()->back();
        }

        $request->session()->flash('success', "Categoria editada com sucesso !!");
        return redirect(route('admin.news.category.list'));

    }

    public function delete (Request $request, $id)
    {

        $category = NewsCategory::find($id)
            ->delete();
        
        if(!$category){
            $request->session()->flash('error', 'Erro ao excluir a categoria !!');
            return redirect()->back();
        }

        $request->session()->flash('success', "Categoria excluida com sucesso !!");
        return redirect(route('admin.news.category.list'));
    }

}
