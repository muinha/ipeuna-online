<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SystemProfile;
use App\Repository\Admin\SystemProfileRepository;
use Illuminate\Http\Request;

class SystemProfileController extends Controller
{
    protected $repository;

    public function __construct(SystemProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function systemProfileList ()
    {
        return view ('admin.system-profile.list');
    }

    public function systemProfileCreate ()
    {
        $profiles = new SystemProfile();

        return view ('admin.system-profile.form', [
            'profiles' => $profiles
        ]);
    }

    public function systemProfileSave (Request $request)
    {
        $request->validate([
            'name' => 'required|unique:system_profiles'
        ]);

        try {
            $profile = $this->repository->saveAction($request);
            
            $request->session()->flash('success', 'Perfil de sistemas, cadastrado com sucesso !!');
            return redirect(route('admin.system.profile.list'));
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Erro ao tentar cadastrar perfil');
            return redirect()->back();
        }

    }

    public function systemProfileEdit ($id)
    {
        $profiles = $this->repository->getSystemProfileAction($id);

        return view ('admin.system-profile.form', [
            'profiles' => $profiles
        ]);
    }

    public function systemProfileUpdate (Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:system_profiles,name,'.$id.'id'
        ]);

        try {
            $profile = $this->repository->updateAction($id, $request);

            $request->session()->flash('success', "Perfil $profile->name, editado com sucesso !!");
            return redirect(route('admin.system.profile.list'));
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Erro ao editar perfil !!');
            return redirect()->back();
        }
        
    }

    public function systemProfileDelete (Request $request, $id)
    {
        try {
            $profile = SystemProfile::find($id);

            if(!$profile){
                $request->session()->flash('error', 'Perfil não encontrado na nossa base de dados !!');
                return redirect()->back();
            }

            $profile->delete();

            $request->session()->flash('success', "Perfil $profile->name, excluido com sucesso !!");
            return redirect(route('admin.system.profile.list'));
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Erro ao excluir perfil !!');
            return redirect()->back();
        }
    }
}
