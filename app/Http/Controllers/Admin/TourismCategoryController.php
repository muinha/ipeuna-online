<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TourismCategory;
use App\Repository\TourismCategoryRepository;
use Illuminate\Http\Request;

class TourismCategoryController extends Controller
{

    protected $repository;

    public function __construct(TourismCategoryRepository $repository)
    {
        
        $this->repository = $repository;

    }

    public function list ()
    {
        $categories = $this->repository::getAllCategory();
        $totCategories = count($categories);

        return view ('admin.tourism.category.list', [
            'categories' => $categories->paginate(10),
            'totCategories' => $totCategories
        ]);
    }

    public function create ()
    {
        $category = new TourismCategory();

        return view ('admin.tourism.category.form', [
            'category' => $category
        ]);
    }

    public function save (Request $request)
    {
        $request->validate([
            'category' => 'required|unique:tourism_categories'
        ]);

        $category = $this->repository->saveAction($request);

        if (!$category) {
            $request->session()->flash('error', 'Erro ao cadastrar categoria !!');
            return redirect()->back();
        }
        
        $request->session()->flash('success', 'Categoria cadastrada com sucesso !!');
        return redirect(route('admin.tourism.category.list'));
    }

    public function edit ($id)
    {
        $category = $this->repository->getCategory($id)
            ->first();
        
        return view ('admin.tourism.category.form', [
            'category' => $category
        ]);
    }

    public function update (Request $request, $id)
    {
        $request->validate([
            'category' => 'required|unique:tourism_categories,category,'.$id.',id'
        ]);
        
        $category = $this->repository->updateAction($request, $id);

        if (!$category) {
            $request->session()->flash('error', 'Erro ao editar categoria !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Categoria editada com sucesso !!');
        return redirect(route('admin.tourism.category.list'));
    }

    public function delete (Request $request, $id)
    {
        $category = TourismCategory::find($id);

        if (!$category) {
            $request->session()->flash('error', 'Erro categoria não encontrada !!');
            return redirect()->back();
        }

        $category->delete();

        if (!$category) {
            $request->session()->flash('error', 'Erro ao deletar categoria !!');
            return redirect()->back();
        }
        
        $request->session()->flash('success', 'Categoria excluida com sucesso !!');
        return redirect(route('admin.tourism.category.list')); 
    }

}
