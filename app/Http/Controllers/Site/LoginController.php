<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm ()
    {
        return view ('site.auth.login-form');
    }

    public function login (Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = $request->get('username');
        $password = $request->get('password');

        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $user = Auth::user();
            $user->last_access = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();

            if($user->system_profile_id !== 2) {
                Auth::logout();
                $request->session()->flash('error', 'Não foi possivel se conectar !!');
                return redirect()->back();
            }

            $request->session()->flash('success', 'Você entrou !!');
            return redirect()->intended('/administracao/inicio');
        }

        $request->session()->flash('error', 'Email ou senha inválidos!!');
        return redirect()->back();
    }

    public function logout () 
    {
        Auth::logout();
        return redirect(route('site.login'));
    }
}
