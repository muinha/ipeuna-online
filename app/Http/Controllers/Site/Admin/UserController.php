<?php

namespace App\Http\Controllers\Site\Admin;

use App\Http\Controllers\Controller;
use App\Repository\Admin\UserAdminRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserAdminRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function userProfile($id)
    {

        $user = $this->repository->getUser($id);

        return view ('site.user.profile', [
            'user' => $user
        ]);
    }
    
    public function userProfileEdit ($id)
    {

        $user = $this->repository->getUser($id);

        return view ('site.admin.user.form', [
            'user' => $user
        ]);
    }

    public function userProfileUpdate($id, Request $request)
    { 
        $request->validate([
            'email' => 'required|unique:users,email,'.$id.',id',
            'username' => 'required|unique:users,username,'.$id.',id',
            'name' => 'required',
            'password' => 'required'
        ]);
        
        try {
            $user = $this->repository->updateAction($id, $request);

            $request->session()->flash('success', 'Perfil Editado com sucesso !!');
        } catch (\Exception $e) {
            $request->session()->flash('error', 'Erro ao editar perfil !!');
        }

        return redirect()->back();

    }

    public function delete ($id, Request $request)
    {
        $user = User::find($id);

        if($user->image_user){
            Storage::delete('/users/'.$user->image_user);
        }

        $user->delete();

        if(!$user){
            $request->session()->flash('error', 'Não foi possivel excluir o usuário !!');
            return redirect()->back();
        }

        $request->session()->flash('success', 'Sua conta foi excluida com sucesso !!');
        return redirect(route('site.index'));
    }

}
