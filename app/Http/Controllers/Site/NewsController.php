<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Repository\Admin\NewsRepository;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function allNews ()
    {
        $idNewsMain = [];

        $newsMain = $this->repository::getAllNews()
            ->where('is_main', 1)
            ->limit(2)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach ($newsMain as $news) {
            $idNewsMain[] = $news->id;
        }

        $allNews = $this->repository::getAllNews()
            ->whereNotIn('news.id', $idNewsMain)
            ->orderBy('created_at', 'desc');

        $newsQtd = $this->repository::getAllNews()
            ->orderBy('qtd_views', 'desc');

        $newsSport = $this->repository::getAllNews()
            ->where('category', 1)
            ->limit(1)
            ->orderBy('created_at', 'desc');

        $newsEntertainment = $this->repository::getAllNews()
            ->where('category', 2)
            ->limit(1)
            ->orderBy('created_at', 'desc');

        $newsPolice = $this->repository::getAllNews()
            ->where('category', 3)
            ->limit(1)
            ->orderBy('created_at', 'desc');

        $newsGov = $this->repository::getAllNews()
            ->where('category', 3)
            ->limit(1)
            ->orderBy('created_at', 'desc');

        return view ('site.news.all-news', [
            'newsMain' => $newsMain,
            'allNews' => $allNews->paginate(6),
            'newsQtd' => $newsQtd->paginate(3),
            'newsSport' => $newsSport->first(),
            'newsEntertainment' => $newsEntertainment->first(),
            'newsPolice' => $newsPolice->first(),
            'newsGov' => $newsGov->first()
        ]);
    }

    public function news ($id)
    {
        $news = $this->repository->getNews($id);

        return view ('site.news.news', [
            'news' => $news
        ]);
    }
}
