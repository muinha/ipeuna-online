<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repository\Admin\UserAdminRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    protected $repository;

    public function __construct(UserAdminRepository $repository)
    {
        $this->repository = $repository;
    }

    public function showRegistrationForm ()
    {
        return view ('site.auth.register');
    }

    public function save (Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users',
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required'
        ]);

        $user = $this->repository->saveAction($request);

        if(!$user) {
            $request->session()->flash('error', 'Erro Verifique as informações abaixo!!');
            return redirect()->back();
        }

        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            $user = Auth::user();
            $user->last_access = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();

            if($user->system_profile_id !== 2) {
                Auth::logout();
                $request->session()->flash('error', 'Não foi possivel se conectar !!');
                return redirect()->back();
            }

            $request->session()->flash('success', 'Você entrou !!');
            return redirect()->intended('/administracao/inicio');
        }

        $request->session()->flash('error', 'Falha ao autenticar !!');
        return redirect()->back();
    }
}
