<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Tourism;
use Illuminate\Http\Request;

class InstitucionalController extends Controller
{
    public function index()
    {
        $news = News::select([
            'id', 'title', 'main_image'
        ])
            ->orderBy('id', 'desc')
            ->limit(8)
            ->get();

        $tourism = Tourism::select([
            'id', 'name', 'description', 'main_image'
        ])
            ->orderBy('id', 'desc')
            ->limit(6)
            ->get();

        return view ('site.index', [
            'tourism' => $tourism,
            'news' => $news
        ]);
    }

    public function contact()
    {
        return view('site.contact.contact');
    }
}
