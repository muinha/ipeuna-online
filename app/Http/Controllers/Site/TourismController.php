<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repository\TourismRepository;
use Illuminate\Http\Request;

class TourismController extends Controller
{
    protected $repository;

    public function __construct(TourismRepository $repository)
    {
        $this->repository = $repository;
    }

    public function allTourism ()
    {
        $tourisms = $this->repository::getAllTourism();

        return view ('site.tourism.all-tourism', [
            'tourisms' =>$tourisms->paginate(9)
        ]);
    }

    public function tourism ($id)
    {
        $tourism = $this->repository->getTourism($id);

        return view ('site.tourism.tourism', [
            'tourism' => $tourism->first()
        ]);
    }
}
