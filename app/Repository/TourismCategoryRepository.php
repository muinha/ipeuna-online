<?php

namespace App\Repository;

use App\Models\TourismCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TourismCategoryRepository extends Model
{

    public static function getAllCategory ()
    {
        return TourismCategory::select('*');
    }

    public function getCategory ($id)
    {
        return TourismCategory::where('id', $id);
    }

    public function saveAction ($request)
    {
        $params = $request->all();

        $category = TourismCategory::create($params);

        return $category;
    }

    public function updateAction ($request, $id)
    {
        $params = $request->all();

        $category = TourismCategory::find($id);
        $category->update($params);
        
        return $category;
    }
    
}
