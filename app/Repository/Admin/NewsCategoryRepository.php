<?php

namespace App\Repository\Admin;

use App\Models\NewsCategory;
use Illuminate\Database\Eloquent\Model;

class NewsCategoryRepository extends Model
{
    
    public static function getAllCategory ()
    {
        return NewsCategory::select('*');
    }

    public function saveAction ($request)
    {
        $params = $request->all();

        $category = NewsCategory::create($params);

        return $category;
    }

    public static function getCategory ($id)
    {
        return NewsCategory::find($id);
    }

    public function updateAction ($request, $id)
    {
        $params = $request->all();

        return NewsCategory::find($id)
            ->update($params);
    }

}
