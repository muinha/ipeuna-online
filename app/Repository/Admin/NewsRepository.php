<?php

namespace App\Repository\Admin;

use App\Models\News;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NewsRepository extends Model
{
    public static function getAllNews ()
    {
        return News::select([
            'news.*', 'u.name'
            ])
            ->join('users as u', 'u.id', '=', 'news.user_id');
    }

    public function getNews ($id)
    {
        return News::where('news.id', $id)
            ->select('news.*', 'u.name')
            ->join('users as u', 'u.id', '=', 'news.user_id')
            ->first();;
    }

    public function saveAction ($request)
    {
        $nameFile = null;

        if ($request->hasFile('main_image') && $request->file('main_image')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->main_image->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->main_image->storeAs('news', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }

        $params = $request->all();

        $news = News::create([
            'title' => $params['title'],
            'subtitle' => $params['subtitle'],
            'main_image' => $nameFile,
            'is_main' => $params['is_main'],
            'category' => $params['category'],
            'user_id' => Auth::user()->id,
            'news' => $params['news']
        ]);

        return $news;
    }

    public function updateAction ($request, $id)
    {
        $nameFile = null;

        if ($request->hasFile('main_image') && $request->file('main_image')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->main_image->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->main_image->storeAs('news', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }

        $params = $request->all();
        $news = News::find($id);

        if($nameFile !== null){
            Storage::delete(['/news/', $news->main_image]);
        }

        $result = $news->update([
            'title' => $params['title'],
            'subtitle' => $params['subtitle'],
            'is_main' => $params['is_main'] ?? null,
            'main_image' => $nameFile !== null ? $nameFile : $news->main_image,
            'category' => $params['category'],
            'user_id' => $news->user_id,
            'news' => $params['news']
        ]);

        return $result;
    }

}
