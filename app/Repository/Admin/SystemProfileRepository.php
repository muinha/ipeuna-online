<?php

namespace App\Repository\Admin;

use App\Models\SystemProfile;
use Illuminate\Database\Eloquent\Model;

class SystemProfileRepository extends Model
{
    public function getSystemProfileAction($id)
    {
        $profile = SystemProfile::find($id);

        return $profile;
    }

    public function saveAction ($request)
    {
        $params = $request->all();
        
        $profile = SystemProfile::create($params);
        
        return $profile;        
    }

    public function updateAction($id, $request)
    {
        $params = $request->all();

        $profile = SystemProfile::find($id);

        if (!$profile) {
            $request->session()->flash('error', 'Perfil não encontrado, na nossa base de dados !!');
            return redirect()->back();
        }

        $profile->update($params);

        return $profile;
    }

}
