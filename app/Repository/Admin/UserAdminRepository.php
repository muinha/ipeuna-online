<?php

namespace App\Repository\Admin;

use App\Models\UserDataPersonal;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserAdminRepository extends Model
{
    
    public static function getAllUsers ()
    {
        return User::select([
            'users.*',
            'sp.name as system_name'
        ])
        ->join('system_profiles as sp', 'sp.id', '=', 'users.system_profile_id');
    }

    public function getUser($id)
    {
        return User::select([
            'users.*',
            'sp.name as system_name',
            'udp.cep',
            'udp.city',
            'udp.states',
            'udp.address',
            'udp.phone',
            'udp.description'
        ])
        ->join('system_profiles as sp', 'sp.id', '=', 'users.system_profile_id')
        ->leftJoin('user_data_personals as udp', 'udp.user_id', '=', 'users.id')
        ->where('users.id', $id)
        ->first();
    }

    public function updateAction ($id, $request)
    {
        $nameFile = null;

        if ($request->hasFile('image_user') && $request->file('image_user')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->image_user->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->image_user->storeAs('users', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }   

        $paramsDataAccount = $request->only([
            'name', 'email', 'image_user', 'username', 'password', 'system_profile_id'
        ]);
        $paramsDataAccount['image_user'] = $nameFile;

        $paramsDataPersonal = $request->only([
            'cep', 'city', 'states', 'address', 'phone', 'description'
        ]);

        $userDataAccount = User::find($id);

        $paramsDataAccount['password'] = $paramsDataAccount['password'] !== $userDataAccount->password ? bcrypt($paramsDataAccount['password']) : $userDataAccount->password;
        $paramsDataAccount['image_user'] = $paramsDataAccount['image_user'] ? $paramsDataAccount['image_user'] : $userDataAccount->image_user;

        if($paramsDataAccount['image_user'] !== $userDataAccount->image_user && $paramsDataAccount['image_user'] !== null) {
            Storage::delete('/users/'.$userDataAccount->image_user);
        }

        try {
            $userDataAccount->update($paramsDataAccount);

            $userDataPersonal = UserDataPersonal::where('user_id', $userDataAccount->id)
                ->first();

            if(!$userDataPersonal){
                UserDataPersonal::create([
                    'user_id' => $id,
                    $paramsDataPersonal
                ]);
            }

            $userDataPersonal->update($paramsDataPersonal);


            return $userDataAccount;            
        } catch (\Exception $e) {
            return false;
        }

    }

    public function saveAction(Request $request)
    {
        $nameFile = null;

        if ($request->hasFile('image_user') && $request->file('image_user')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->image_user->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->image_user->storeAs('users', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }   

        $paramsDataAccount = $request->only([
            'name', 'email', 'image_user', 'username', 'password', 'system_profile_id'
        ]);
        $paramsDataAccount['image_user'] = $nameFile;
        $paramsDataAccount['password'] = bcrypt($paramsDataAccount['password']);

        $paramsDataPersonal = $request->only([
            'cep', 'city', 'states', 'address', 'phone', 'description'
        ]);

        try {
            $user = User::create($paramsDataAccount);

            UserDataPersonal::create([
                'user_id' => $user->id,
                $paramsDataPersonal
            ]);

            return $user;
        } catch (\Exception $e) {
            return false;
        }
        
    }

}
