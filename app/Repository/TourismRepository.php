<?php

namespace App\Repository;

use App\Models\Tourism;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TourismRepository extends Model
{

    public static function getAllTourism ()
    {
        return DB::table('tourisms as t')
            ->select('t.*', 'u.name as user_name', 'c.category')
            ->leftJoin('tourism_categories as c', 'c.id', '=', 't.category')
            ->leftJoin('users as u', 'u.id', '=', 't.user_id');
    }

    public function getTourism ($id)
    {
        return Tourism::where('id', $id);
    }

    public function saveAction ($request)
    {
        $nameFile = null;

        if ($request->hasFile('main_image') && $request->file('main_image')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->main_image->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->main_image->storeAs('tourism', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }

        $params = $request->all();

        $tourism = Tourism::create([
            'name' => $params['name'],
            'localization' => $params['localization'],
            'contains' => $params['contains'],
            'pay' => $params['pay'],
            'description' => $params['description'],
            'category' => $params['category'],
            'main_image' => $nameFile,
            'user_id' => Auth::user()->id
        ]);

        return $tourism;
    }

    public function updateAction ($request, $id)
    {
        $nameFile = null;

        if ($request->hasFile('main_image') && $request->file('main_image')->isValid()){
            $name = uniqid(date('ymdhis'));
            $extension = $request->main_image->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->main_image->storeAs('news', $nameFile);

            if(!$upload) {
                return redirect()->back();
            }
        }

        $params = $request->all();
        $tourism = Tourism::find($id);

        if($nameFile !== null){
            Storage::delete(['/tourism/', $tourism->main_image]);
        }

        $result = $tourism->update([
            'main_image' => $nameFile !== null ? $nameFile : $tourism->main_image,
            'name' => $params['name'],
            'localization' => $params['localization'],
            'contains' => $params['contains'],
            'pay' => $params['pay'],
            'description' => $params['description'],
            'category' => $params['category'],
            'user_id' => Auth::user()->id
        ]);

        return $result;
    }

}
