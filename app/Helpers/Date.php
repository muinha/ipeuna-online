<?php

namespace App\Helpers;

use Carbon\Carbon;

class Date
{

    /**
     * @var array
     */
    public static $months = [
        '01' => 'jan',
        '02' => 'feb',
        '03' => 'mar',
        '04' => 'apr',
        '05' => 'may',
        '06' => 'jun',
        '07' => 'jul',
        '08' => 'aug',
        '09' => 'sep',
        '10' => 'oct',
        '11' => 'nov',
        '12' => 'dec',
    ];

    /**
     * Conversor o formato da data de dd/mm/aaaa para aaaa/mm/dd e vice-versa
     *
     * @param string $date dd/mm/aaaa | aaaa-mm-dd [yy:m:ss]
     * @return string
     */
    public static function conversion($date)
    {
        if (strlen($date)) {
            $time = null;
            if (strlen($date) > 10) {
                $time = substr($date, 10);
                $date = substr($date, 0, 10);
            }

            $token = strpos($date, '/') ? '/' : '-';
            $tmp = explode($token, $date);
            foreach ($tmp as &$val) {
                $val = str_pad($val, 2, 0, STR_PAD_LEFT);
            }

            return trim(implode(($token == '-' ? '/' : '-'), array_reverse($tmp)) . " {$time}");
        }

        return false;
    }

    public static function conversionOnlyDate($date)
    {
        if (strlen($date)) {
            $time = null;
            if (strlen($date) > 10) {
                $date = substr($date, 0, 10);
            }

            $token = strpos($date, '/') ? '/' : '-';
            $tmp = explode($token, $date);
            foreach ($tmp as &$val) {
                $val = str_pad($val, 2, 0, STR_PAD_LEFT);
            }

            return trim(implode(($token == '-' ? '/' : '-'), array_reverse($tmp)));
        }

        return false;
    }

    /**
     * @param $year
     * @return array
     */
    public static function Holidays($year)
    {
        $day = 86400;
        $dates = array();
        $dates['easter'] = easter_date($year);
        $dates['good_friday'] = $dates['easter'] - (2 * $day);
        $dates['carnival'] = $dates['easter'] - (47 * $day);
        $dates['corpus_christi'] = $dates['easter'] + (60 * $day);
        $holidays = array(
            Carbon::create($year, 1, 1)->format('Y-m-d'),
            date('Y-m-d', $dates['carnival']),
            date('Y-m-d', $dates['good_friday']),
            Carbon::create($year, 4, 21)->format('Y-m-d'),
            Carbon::create($year, 5, 1)->format('Y-m-d'),
            date('Y-m-d', $dates['corpus_christi']),
            Carbon::create($year, 7, 9)->format('Y-m-d'),
            Carbon::create($year, 9, 7)->format('Y-m-d'),
            Carbon::create($year, 10, 12)->format('Y-m-d'),
            Carbon::create($year, 11, 2)->format('Y-m-d'),
            Carbon::create($year, 11, 15)->format('Y-m-d'),
            Carbon::create($year, 11, 20)->format('Y-m-d'),
            Carbon::create($year, 12, 25)->format('Y-m-d'),
        );

        return $holidays;
    }

    /**
     * @param $date
     * @return bool
     */
    public static function isHoliday($date)
    {
        $carbonDate = Carbon::createFromFormat('Y-m-d', $date);
        $holydays = self::Holidays($carbonDate->year);
        if (in_array($date, $holydays)) {
            return true;
        }

        return false;
    }

    /**
     * @param $date
     * @return bool
     */
    public static function isWeekend($date)
    {
        $carbonDate = Carbon::createFromFormat('Y-m-d', $date);
        $dayOfWeek = $carbonDate->dayOfWeek;

        if ($dayOfWeek == 0 || $dayOfWeek == 6) {
            return true;
        }

        return false;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return array
     */
    public static function getDatesFromRange($fromDate, $toDate)
    {
        $start = Carbon::createFromFormat('Y-m-d', substr($fromDate, 0, 10));
        $end = Carbon::createFromFormat('Y-m-d', substr($toDate, 0, 10));

        $dates = [];
        while ($start->lte($end)) {
            $dates[] = $start->copy()->format('Y-m-d');
            $start->addDay();
        }

        return $dates;
    }

    /**
     * @param $seconds
     * @param string $separator
     * @return string
     */
    public static function convertSecondsToTime($seconds, $separator = ':') // t = seconds, f = separator
    {
        return sprintf(
            "%02d%s%02d%s%02d",
            floor($seconds / 3600),
            $separator,
            ($seconds/60) % 60, $separator, $seconds % 60
        );
    }

    /**
     * @param $time
     * @return mixed
     */
    public static function convertTimeToSeconds($time)
    {
        return strtotime($time) - strtotime('TODAY');
    }

    /**
     * @param $stringDate
     * @return bool|string
     */
    public static function getDayFromString($stringDate)
    {
        return substr($stringDate, 8);
    }

    /**
     * @param $stringDate
     * @return bool|string
     */
    public static function getYearFromString($stringDate)
    {
        return substr($stringDate, 0, 4);
    }


    /**
     * @param $day
     * @param bool $convertBR
     * @return string
     */
    public static function getFullNextDataByDay($day, $convertBR = true)
    {
        if ($day > 31 || $day <= 0) {
            $day = 1;
        }

        $dateBase = Carbon::now();
        $currentDay = $dateBase->day;

        if ($day == $currentDay) {
            return $dateBase->format('d/m/Y');
        }

        if ($day < $dateBase->day) {
            $dateBase->day = $day;
            $dateBase->addMonth();
        }

        $lastMonthDay = $dateBase->endOfMonth();

        if ($day > $lastMonthDay->day) {
            $day = $lastMonthDay->day;
        }

        $newDateString = sprintf(
            '%s-%s-%s',
            $dateBase->year,
            $dateBase->month,
            $day
        );

        $date = Carbon::createFromFormat('Y-m-d', $newDateString);
        if ($convertBR) {
            return $date->format('d/m/Y');
        }

        return $date->format('Y-m-d');
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }

    /**
     * @param int $quantity
     * @param bool $returnWithCurrentMonth
     * @param string $order
     * @param int $qtdSkipCurrentMonth
     * @return array
     */
    public static function getLastMonths(
        $quantity = 6,
        $returnWithCurrentMonth = false,
        $order = 'desc',
        $qtdSkipCurrentMonth = 0
    )

    {
        $data = [];
        for ($i = 0; $i < $quantity; $i++) {
            $newDate = Carbon::now()
                ->day(true)
                ->subMonths($returnWithCurrentMonth ? ($i + $qtdSkipCurrentMonth) : ($i + 1));
            $data[] = [
                'label' => Labels::$months[$newDate->month] . '/' . $newDate->year,
                'value' => $newDate->month . '-' . $newDate->year,
                'month_year' => $newDate->year . (strlen($newDate->month) < 2 ? '0' . $newDate->month : $newDate->month)
            ];
        }

        if ($order == 'desc') {
            return $data;
        }

        return array_reverse($data);
    }

    public static function getLastYears($qtdYears = 5)
    {

        $newDate = Carbon::now();
        $year = $newDate->year;
        $data = [];

        for($i = 0; $i < $qtdYears; $i++){
            $data[] = $year --;
        }

        return $data;

    }

    /**
     * @return array
     */
    public static function getAllMounths()
    {
        $data = [];

        foreach(Labels::$months as $key => $value){
            $data[] = [
                'key'   => strlen($key) < 2 ? '0'.$key : $key,
                'value' => $value
            ];
        }

        return $data;
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return int
     */
    public static function countMonthBetweenTwoDates($start_date, $end_date)
    {
        $start_date = self::conversion($start_date);
        $end_date = self::conversion($end_date);

        $start_date = explode('-', $start_date);
        $end_date = explode('-', $end_date);

        $startDate = Carbon::createFromDate($start_date[0], $start_date[1], $start_date[2]);
        $endDate = Carbon::createFromDate($end_date[0], $end_date[1], $end_date[2]);

        $months = $startDate->diffInMonths($endDate);

        return $months;
    }

    /**
     * @param $competence
     * @return array
     */
    public static function getFirstAndLastDateOfCompetence($competence)
    {
        $competence = explode('-', $competence);

        $date = new Carbon();

        $date->month($competence[0])->year($competence[1]);

        return [
            'first' => $date->firstOfMonth()->format('d/m/Y'),
            'last' => $date->lastOfMonth()->format('d/m/Y')
        ];
    }

    /**
     * @param $date
     * @param bool $withTime
     * @return bool|string|string[]|null
     */
    public static function onlyDate($date, $withTime = false)
    {
        $date = onlyNumbers($date);
        $date = substr($date, 0, $withTime ? 14 : 8);

        if ($date == '00000000') {
            return '';
        }

        return $date;
    }

}
