<?php

use Illuminate\Database\Seeder;

class TourismCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0;$i < 20; $i++) {
            DB::table('tourism_categories')->insert([
                'category' => Str::random(10)
            ]);
        }
    }
}
