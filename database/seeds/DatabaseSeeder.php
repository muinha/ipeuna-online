<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(NewsCategoryTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(TourismCategoryTableSeeder::class);
        $this->call(TourismTableSeeder::class);
    }
}
