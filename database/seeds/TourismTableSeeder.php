<?php

use Illuminate\Database\Seeder;

class TourismTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number = [
            1,2,3,4,5,6,7,8,9
        ];

        for ($i = 0;$i < 20; $i++) {
            DB::table('tourisms')->insert([
                'user_id' => 1,
                'category' => array_rand($number),
                'name' => Str::random(100),
                'main_image' => Str::random(10),
                'localization' => Str::random(100),
                'contains' => Str::random(10).', ' .Str::random(10),
                'pay' => 1,
                'description' => Str::random(1200)
            ]);
        }
    }
}
