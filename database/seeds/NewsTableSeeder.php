<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            DB::table('news')->insert([
                'user_id' => 1,
                'title' => Str::random(120),
                'subtitle' => Str::random(120),
                'category' => 3,
                'main_image' => Str::random(10),
                'news' => Str::random(5000)
            ]);
        }
    }
}
