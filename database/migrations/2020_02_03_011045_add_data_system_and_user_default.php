<?php

use App\Models\SystemProfile;
use App\Models\UserDataPersonal;
use App\User;
use Illuminate\Database\Migrations\Migration;

class AddDataSystemAndUserDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SystemProfile::create(['name' => 'Administrador']);
        SystemProfile::create(['name' => 'Membro']);

        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'system_profile_id' => 1,
            'password' => bcrypt('445533')
        ]);

        UserDataPersonal::create([
            'user_id' => $user->id,
            'cep' => '13537000',
            'city' => 'São Paulo',
            'states' => 'São Paulo',
            'address' => 'Avenida: paulista',
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
